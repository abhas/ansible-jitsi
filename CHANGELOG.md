# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Allow configuration of ssl cert+key path
- [jitsiexporter](https://github.com/xsteadfastx/jitsiexporter) for JVB monitoring

### Changed

- Fixed secure domain config
- Pinned prosody version to 0.11.5-1
- Use jitsi stable branch by default

### Removed

## [0.1.0] - 2020-04-13

### Added

- This CHANGELOG
- First version

### Changed

n/a

### Removed

n/a

[Unreleased]: https://gitlab.com/guardianproject-ops/ansible-jitsi/compare/0.1.0...HEAD
[0.1.0]: https://gitlab.com/guardianproject-ops/ansible-jitsi/tag/0.1.0
